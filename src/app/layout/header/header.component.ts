import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { WelcomeService } from '../../views/welcome/welcome.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private welcomeServcie: WelcomeService) { }

  ngOnInit() {
  }

  onClickAppName() {
    this.welcomeServcie.onResetInput();
  }

}
