import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { GET_TIMELINE_START, GET_TIMELINE_SUCCESS, GET_TIMELINE_ERROR, CLEAR_TIMELINE, TimelineState } from './welcome.reducer';
import { AppState } from './../../main-reducer';
import { map, switchMap, retryWhen, delayWhen, tap, catchError, concatMap, pluck } from 'rxjs/operators';
import { AuthState } from '../../app.reducer';
import { Subject, throwError, of, Subscription, merge, concat } from 'rxjs';

@Injectable()
export class WelcomeService {

    private _resetInput = new Subject();
    resetInput$ = this._resetInput.asObservable();

    constructor(private http: HttpClient, private store: Store<AppState>) { }

    searchTimeline(name: string) {

        const getTimelineStart$ = of(this.store.dispatch({ type: GET_TIMELINE_START }));
        const clearTimeline$ = of(this.store.dispatch({ type: CLEAR_TIMELINE }));

        const start$ = merge(getTimelineStart$, clearTimeline$);

        const getTimelineSuccess = (data) => of(this.store.dispatch({ type: GET_TIMELINE_SUCCESS, payload: data }));
        const getTimelineFail = (err) => of(this.store.dispatch({ type: GET_TIMELINE_ERROR, payload: err }));

        const $store = this.store.pipe(
            select('auth'),
            pluck('token'),
            concatMap((token: string) => this.getTimeline(name, token)),
            tap(console.log),
            concatMap(data => getTimelineSuccess(data)),
            catchError(err => getTimelineFail(err))
        );

        return concat(start$, $store);
    }

    getTimeline(name: string, token: string) {
        return this.http.get('http://localhost:9000/api/timeline?name=' + name, { headers: {Authorization: token }});
    }

    onResetInput() {
        this._resetInput.next();
    }

}
