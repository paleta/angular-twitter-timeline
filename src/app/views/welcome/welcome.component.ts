import { MatPaginator, MatSnackBar } from '@angular/material';
import { map, debounceTime, distinctUntilChanged, delay, delayWhen, filter, switchMap, concatMap } from 'rxjs/operators';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { fromEvent } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { WelcomeService } from './welcome.service';
import { AppState } from './../../main-reducer';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild('timelineInput') input: ElementRef;
  @ViewChild('searchContainer') searchContainer: ElementRef;
  @ViewChild('filterInput') filterInput: ElementRef;
  @ViewChild('timelineContainer') timelineContainer: ElementRef;

  tweets: any[] = [];
  paginatedTweets: any[] = [];
  search = '';
  dataLength: number;
  busy = false;

  constructor(private welcomeServcie: WelcomeService,
              private store: Store<AppState>,
              private renderer: Renderer2,
              public snackBar: MatSnackBar) { }

  ngOnInit() {

    this.welcomeServcie.resetInput$
      .subscribe(() => {
        this.onResetInputs();
      });
  }

  ngAfterViewInit() {

    const geTimeline$ = this.store.pipe(
      select('timeline'),
      delay(0),
      delayWhen(() => this.paginator.initialized)
    );

    const searchTimeline$ = fromEvent<any>(this.input.nativeElement, 'keyup')
      .pipe(
        map(event => event.target.value),
        filter(value => value !== ''),
        debounceTime(400),
        distinctUntilChanged(),
        switchMap(value => this.welcomeServcie.searchTimeline(value)),
        concatMap(() => geTimeline$)
      );

      searchTimeline$.subscribe(data => {

        if (data.tweets.length !== 0) {
          this.renderer.addClass(this.searchContainer.nativeElement, 'search--top');
          this.renderer.addClass(this.timelineContainer.nativeElement, 'timeline--top');
        } else {
          this.renderer.removeClass(this.searchContainer.nativeElement, 'search--top');
          this.renderer.removeClass(this.timelineContainer.nativeElement, 'timeline--top');
        }
        this.busy = data.busy;
        this.tweets = data.tweets;
        this.dataLength = data.tweets.length;
        this.filterTweets(0);

        if (!!data.error) {
          this.openSnackBar('Timeline not found: ' + data.error.message);
        }
    });


    fromEvent<any>(this.filterInput.nativeElement, 'keyup')
      .pipe(
        map(event => event.target.value),
        debounceTime(400),
        distinctUntilChanged()
      ).subscribe((search: string) => {
        this.search = search;
        this.filterTweets(0);
      });

  }

  filterTweets(page?) {

    if (page === 0) {
      this.paginator.pageIndex = page;
    }

    const maxLength = this.paginator.pageSize  * (this.paginator.pageIndex + 1);
    const minLength = this.paginator.pageIndex * this.paginator.pageSize;

    const tweets = this.tweets
      .filter((tweet: any) => {
        if (tweet.text.toLowerCase().indexOf(this.search.toLowerCase()) !== -1) {
          return tweet;
        }
      });

    this.dataLength = tweets.length;

    this.paginatedTweets = tweets.slice(minLength, maxLength);

    // console.log(this.paginatedTweets, tweets);

  }

  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 4000,
    });
  }

  onResetInputs() {
    this.search = '';
    this.input.nativeElement.value = '';
    this.filterInput.nativeElement.value = '';
  }


}
