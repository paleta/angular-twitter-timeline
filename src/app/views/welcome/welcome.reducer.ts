export const GET_TIMELINE_START = 'GET_TIMELINE_START';
export const GET_TIMELINE_SUCCESS = 'GET_TIMELINE_SUCCESS';
export const GET_TIMELINE_ERROR = 'GET_TIMELINE_ERROR';

export const CLEAR_TIMELINE = 'CLEAR_TIMELINE';

export interface TimelineState {
    busy: boolean;
    error: any;
    tweets: any[];
}

const initialState = {
    busy: false,
    error: null,
    tweets: []
};

export function timelineReducer(state: TimelineState = initialState, action) {
  switch (action.type) {

    case GET_TIMELINE_START:
        return {
            ...state,
            busy: true,
            error: null,
        };
    case GET_TIMELINE_SUCCESS:
        return {
            ...state,
            busy: false,
            error: null,
            tweets: [
                ...action.payload
            ]
        };
    case GET_TIMELINE_ERROR:
        return {
            ...state,
            busy: false,
            error: action.payload,
        };
    case CLEAR_TIMELINE:
        return {
            ...state,
            tweets: [],
        };
    default:
      return state;
  }
}
