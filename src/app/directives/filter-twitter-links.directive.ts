import { Directive, ElementRef, Renderer2, OnInit, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appFilterTwitterLinks]'
})
export class FilterTwitterLinksDirective implements AfterViewInit {

  constructor(private elRef: ElementRef, private renderer: Renderer2) {}


  ngAfterViewInit() {

    let value = this.elRef.nativeElement.innerHTML;

    const hashTags = value.match(/(^|\B)#(?![0-9_]+\b)([a-zA-Z0-9_]{1,30})(\b|\r)/ig) || [];

    hashTags.forEach(element => {
      value = value.replace(element, `<a target="_blank" href="https://twitter.com/hashtag/${element.split('#')[1]}">${element}</a>`);
    });

    const users = value.match(/(^|[^@\w])@(\w{1,15})\b/ig) || [];

    users.forEach(element => {
      value = value.replace(element, `<a target="_blank" href="https://twitter.com/${element.split('@')[1]}">${element}</a>`);
    });

    this.elRef.nativeElement.innerHTML = value;

  }

}
