export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_ERROR = 'AUTH_ERROR';

export interface AuthState {
    busy: boolean;
    error: any;
    token: string;
}

const initialState = {
    busy: false,
    error: null,
    token: null
};

export function authReducer(state: AuthState = initialState, action) {
  switch (action.type) {

    case AUTH_START:
        return {
            ...state,
            busy: true,
            error: null,
        };
    case AUTH_SUCCESS:
        return {
            ...state,
            busy: false,
            error: null,
            token: action.payload
        };
    case AUTH_ERROR:
        return {
            ...state,
            busy: false,
            error: action.payload,
        };
    default:
      return state;
  }
}
