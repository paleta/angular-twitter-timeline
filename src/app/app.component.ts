import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, concatMap } from 'rxjs/operators';

import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';


  constructor(private http: HttpClient, private appService: AppService) { }

  ngOnInit() {
    this.appService.checkAuthStatus();
  }

}
