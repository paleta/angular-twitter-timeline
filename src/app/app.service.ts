import { map, concatMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from './main-reducer';
import { AUTH_START, AUTH_SUCCESS, AUTH_ERROR } from './app.reducer';

@Injectable()

export class AppService {

    authSub: Subscription;

    constructor(private http: HttpClient, private store: Store<AppState>) { }


    checkAuthStatus() {
        const token = localStorage.getItem('token');

        if (token) {
            this.store.dispatch({ type: AUTH_SUCCESS, payload: token });
        } else {
            this.authenticate();
        }

    }

    authenticate() {

        this.store.dispatch({ type: AUTH_START });

        this.authSub = this.http.post('http://localhost:9000/api/auth', '')
            .pipe(map((data: any) => data.access_token))
            .subscribe((token: string) => {
                localStorage.setItem('token', token);
                this.store.dispatch({ type: AUTH_SUCCESS, payload: token });
            }, (error: any) => {
                this.store.dispatch({ type: AUTH_ERROR, payload: error });
            });

    }

    unsubscribeAuthSub() {

        this.authSub.unsubscribe();
    }

}
