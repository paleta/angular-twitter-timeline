import { TimelineState, timelineReducer } from './views/welcome/welcome.reducer';
import { AuthState, authReducer } from './app.reducer';

import { ActionReducerMap } from '@ngrx/store';

export interface AppState {
    timeline: TimelineState;
    auth: AuthState;
}

const reducers = {
    timeline: timelineReducer,
    auth: authReducer,
};

export const reducer: ActionReducerMap<AppState> = reducers;

// export const reducer: ActionReducer<AppState> = combineReducers(reducers);
