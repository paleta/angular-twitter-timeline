const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const request = require('request');

const config = require('./config');


const app = express();
app.use(bodyParser.json());
app.use(cors());

app.post('/api/auth', (req, res) => {
  
    const encode_secret = new Buffer(config.consumerKey + ':' + config.consumerSecret).toString('base64');

    const options = {
        url: 'https://api.twitter.com/oauth2/token',
        headers: {
            'Authorization': 'Basic ' + encode_secret,
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
        body: 'grant_type=client_credentials'
    };

    request.post(options, function(error, response, body) {

        if (!error && response.statusCode == 200) {
            res.status(200).send(body);
        } else {
            res.status(400).send(error);
        }

    });

});


app.get('/api/timeline', (req, res) => {

    const timelineName = req.query.name;
    const token = req.headers['authorization'];

    const options = {
        url: 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=' + timelineName + '&count=200',
        headers: {
            'Authorization': 'Bearer ' + token,
        }
    };

    setTimeout(() => {
        request(options, function(error, response, body) {

            console.log(response.statusMessage);
   
           if (!error && response.statusCode == 200) {
               res.status(200).send(body);
           } else {
               res.status(response.statusCode).send();
           }
   
       });
    }, 2000);



});

app.listen(9000, () => {console.log('Server is running on port 9000')}); 
